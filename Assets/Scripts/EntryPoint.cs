﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class EntryPoint : MonoBehaviour
    {
        [SerializeField] private SceneDependencies dependencies;

		private GameStateMachine gameStateMachine;

		private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);

            var gameContent = GetGameContent();

            var nickGenerator = new NickGenerator();
            var idGenerator = new IdGenerator();
            var battleGatherer = new BattleGatherer(nickGenerator, idGenerator);

            gameStateMachine = new GameStateMachine(new List<IGameState>()
            {
                new LoadingState(dependencies.LoadingWindow),
                new BattleState(
                    gameContent,
                    dependencies,
                    battleGatherer)
            });

            PrepareAllServices();

            gameStateMachine.ChangeState<LoadingState>(context: null);
        }

		private void OnDestroy()
        {
            gameStateMachine.Deinit();
        }

        private void PrepareAllServices()
        {
            dependencies.LoadingWindow.Hide();
            dependencies.BattleWindow.Hide();
        }

        private Data GetGameContent()
        {
            var contentLoader = new ResourceContentLoader("data");

            var rawContent = contentLoader.LoadContent();

            var contentParser = new JsonContentParser(rawContent);

            return contentParser.ParseContent();
        }
    }
}