﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
	public class UIBattleWindow : UIWndowBase<UIBattleWindowModel>
	{
		[SerializeField] private PlayerParametersViews parametersViews;
		[SerializeField] private UIParametersPanel parametersPanel;
		[SerializeField] private Button attackButton;
		[SerializeField] private Button prevButton;
		[SerializeField] private Button nextButton;
		[SerializeField] private Button useBuffsRestartButton;
		[SerializeField] private Button noBuffsRestartButton;

		public override void Show(UIBattleWindowModel windowModel)
		{
			base.Show(windowModel);

			attackButton.onClick.AddListener(AttackButtonHandler);
			prevButton.onClick.AddListener(PrevButtonHandler);
			nextButton.onClick.AddListener(NextButtonHandler);
			useBuffsRestartButton.onClick.AddListener(UseBuffsHandler);
			noBuffsRestartButton.onClick.AddListener(NoBuffsHandler);

			var allPlayers = windowModel.GetAllPlayers();

			parametersViews.Init(allPlayers, windowModel.GameContent, windowModel.BattleCamera);
			parametersPanel.Init(windowModel.BattleModel, windowModel.GameContent);
		}

		public override void Hide()
		{
			base.Hide();

			attackButton.onClick.RemoveListener(AttackButtonHandler);
			prevButton.onClick.RemoveListener(PrevButtonHandler);
			nextButton.onClick.RemoveListener(NextButtonHandler);
			useBuffsRestartButton.onClick.RemoveListener(UseBuffsHandler);
			noBuffsRestartButton.onClick.RemoveListener(NoBuffsHandler);

			parametersViews.Deinit();
			parametersPanel.Deinit();
		}

		private void AttackButtonHandler()
		{
			WindowModel.Attack();
		}

		private void PrevButtonHandler()
		{
			WindowModel.SwitchPlayer(false);
		}

		private void NextButtonHandler()
		{
			WindowModel.SwitchPlayer(true);
		}

		private void UseBuffsHandler()
		{
			WindowModel.Restart(useBuffs: true);
		}

		private void NoBuffsHandler()
		{
			WindowModel.Restart(useBuffs: false);
		}
	}
}