﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
	public class UIBattleWindowModel
	{
		public event Action<bool> RestartRequested;

		public Camera BattleCamera { get; }
		public Data GameContent { get; }
		public BattleModel BattleModel { get; }

		public UIBattleWindowModel(BattleModel battleModel, Data gameContent, Camera battleCamera)
		{
			BattleModel = battleModel;
			GameContent = gameContent;
			BattleCamera = battleCamera;
		}

		public List<PlayerModel> GetAllPlayers()
		{
			return BattleModel.Players;
		}

		public void SwitchPlayer(bool delta)
		{
			var direction = delta ? 1 : -1;

			var index = BattleModel.Players.FindIndex(player => player.Id == BattleModel.CurrentPlayer.Value);

			var validIndex = (index + direction + BattleModel.Players.Count) % BattleModel.Players.Count;

			BattleModel.CurrentPlayer.Value = BattleModel.Players[validIndex].Id;
		}

		public void Attack()
		{
			var currentPlayer = BattleModel.Players.Find(player => player.Id == BattleModel.CurrentPlayer.Value);

			var enemies = GetEnemyModels(BattleModel.Players);

			foreach (var enemy in enemies)
			{
				currentPlayer.Controller.WeaponController.Attack(enemy);
			}
		}

		public void Restart(bool useBuffs)
		{
			RestartRequested?.Invoke(useBuffs);
		}

		private List<PlayerModel> GetEnemyModels(List<PlayerModel> players)
		{
			return players.Where(player => player.Id != BattleModel.CurrentPlayer.Value).ToList();
		}
	}
}