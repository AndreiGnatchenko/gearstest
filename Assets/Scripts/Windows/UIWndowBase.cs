﻿using UnityEngine;

public class UIWndowBase<T> : MonoBehaviour
{
	[SerializeField] private CanvasGroup canvasGroup;

	protected T WindowModel { get; private set; }

	public virtual void Show(T windowModel)
	{
		WindowModel = windowModel;

		Toggle(true);
	}

	public virtual void Hide()
	{
		Toggle(false);
	}

	private void Toggle(bool state)
	{
		canvasGroup.alpha = state ? 1.0f : 0.0f;
		canvasGroup.blocksRaycasts = state;
	}
}
