﻿using System.Collections.Generic;

namespace Support
{
	public static class Support
	{
        public static T Random<T>(this List<T> list)
        {
            var randomIndex = UnityEngine.Random.Range(0, list.Count);

            return list[randomIndex];
        }

        public static List<T> Shuffle<T>(this List<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                int k = UnityEngine.Random.Range(0, n);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }
    }
}