﻿using UnityEngine;

namespace Support
{
	public static class SystemTime
	{
		public static float GetCurrentTime() => Time.time;
	}
}