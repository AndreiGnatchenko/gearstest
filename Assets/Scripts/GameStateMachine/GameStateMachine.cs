﻿using System;
using System.Collections.Generic;

namespace Game
{
	public class GameStateMachine
	{
		private IGameState currentState;
		private List<IGameState> states;

		public GameStateMachine(List<IGameState> states)
		{
			this.states = states;
		}

		public void ChangeState<T>(object context)
		{
			var state = GetState(typeof(T));

			if (currentState != null)
			{
				currentState.Deinit();
			}

			currentState = state;

			currentState.Init(this, context);
		}

		private IGameState GetState(Type type)
		{
			return states.Find(item => item.GetType() == type);
		}

		public void Init()
		{

		}

		public void Deinit()
		{
			if (currentState != null)
			{
				currentState.Deinit();
			}
		}
	}
}
