﻿namespace Game
{
	public interface IGameState
	{
		void Init(GameStateMachine machine, object context);
		void Deinit();
	}
}