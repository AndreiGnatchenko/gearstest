﻿using System;

namespace Game
{
	public class BattleState : IGameState
	{
		private UILoadingWindow loadingWindow;
		private UIBattleWindow battleWindow;

		private Data gameContent;
		private SceneDependencies dependencies;
		private BattleGatherer battleGetherer;
		private BattleController battleController;
		private BattleModel battleModel;
		private UIBattleWindowModel battleWindowModel;
		private GameStateMachine machine;

		public BattleState(Data gameContent, SceneDependencies dependencies, BattleGatherer battleGetherer)
		{
			loadingWindow = dependencies.LoadingWindow;
			battleWindow = dependencies.BattleWindow;
			this.gameContent = gameContent;
			this.dependencies = dependencies;
			this.battleGetherer = battleGetherer;
		}

		public void Init(GameStateMachine machine, object context)
		{
			this.machine = machine;

			var useBuffs = (bool)context;

			var buffController = new BuffController(gameContent, useBuffs);

			var playerDatas = buffController.GetPlayersStats(gameContent.settings.playersCount, gameContent.stats);

			battleModel = battleGetherer.Gather(playerDatas);

			battleController = new BattleController(battleModel, dependencies.SpawnPoints, dependencies.PlayerPrefab);

			battleController.Init();

			battleWindowModel = new UIBattleWindowModel(battleModel, gameContent, dependencies.BattleCamera);
			battleWindowModel.RestartRequested += OnRestartRequested;

			battleWindow.Show(battleWindowModel);

			dependencies.CurrentPlayerView.Init(battleModel);
			dependencies.CameraController.Init(gameContent);

			loadingWindow.Hide();
		}

		public void Deinit()
		{
			loadingWindow.Show(new UILoadingWindowModel());

			battleWindow.Hide();

			battleWindowModel.RestartRequested -= OnRestartRequested;

			dependencies.CurrentPlayerView.Deinit();
			dependencies.CameraController.Deinit();

			battleController.Deinit();
		}

		private void OnRestartRequested(bool useBuffs)
		{
			machine.ChangeState<BattleState>(context: useBuffs);
		}
	}
}