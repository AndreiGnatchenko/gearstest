﻿using UnityEngine;

namespace Game
{
	public class LoadingState : IGameState
	{
		private const int TargetFrameRate = 60;

		private UILoadingWindow loadingWindow;

		public LoadingState(UILoadingWindow loadingWindow)
		{
			this.loadingWindow = loadingWindow;
		}

		public void Init(GameStateMachine machine, object context)
		{
			var loadingWindowModel = new UILoadingWindowModel();

			loadingWindow.Show(loadingWindowModel);

			Application.targetFrameRate = TargetFrameRate;

			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			machine.ChangeState<BattleState>(context: true);
		}

		public void Deinit()
		{
		}
	}
}