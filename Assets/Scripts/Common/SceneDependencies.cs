﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class SceneDependencies : MonoBehaviour
	{
		[SerializeField] private UILoadingWindow loadingWindow;
		[SerializeField] private UIBattleWindow battleWindow;
		[SerializeField] private PlayerController playerPrefab;
		[SerializeField] private List<Transform> spawnPoints;
		[SerializeField] private Camera battleCamera;
		[SerializeField] private CurrentPlayerView currentPlayerView;
		[SerializeField] private CameraController cameraController;

		public UILoadingWindow LoadingWindow => loadingWindow;
		public UIBattleWindow BattleWindow => battleWindow;
		public PlayerController PlayerPrefab => playerPrefab;
		public List<Transform> SpawnPoints => spawnPoints;
		public Camera BattleCamera => battleCamera;
		public CurrentPlayerView CurrentPlayerView => currentPlayerView;
		public CameraController CameraController => cameraController;
	}
}