﻿using Support;
using System.Collections.Generic;

namespace Game
{
	public class BattleGatherer
	{
		private INickGenerator nickGenerator;
		private IIdGenerator idGenerator;

		public BattleGatherer(INickGenerator nickGenerator, IIdGenerator idGenerator)
		{
			this.nickGenerator = nickGenerator;
			this.idGenerator = idGenerator;
		}

		public BattleModel Gather(List<PlayerInitData> playerDatas)
		{
			var players = new List<PlayerModel>();

			foreach (var playerData in playerDatas)
			{
				var playerModel = new PlayerModel(idGenerator.GenerateId(), nickGenerator.GenerateNick(), playerData);

				players.Add(playerModel);
			}

			var player = players.Random();

			return new BattleModel(players, player.Id);
		}
	}
}