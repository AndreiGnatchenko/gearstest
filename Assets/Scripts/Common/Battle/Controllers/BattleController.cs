﻿
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class BattleController
	{
		private PlayerBuilderController playerBuilderController;

		public BattleController(BattleModel battleModel, List<Transform> spawnPoints, PlayerController playerPrefab)
		{
			var spawnController = new SpawnController(playerPrefab);

			playerBuilderController = new PlayerBuilderController(battleModel.Players, spawnController, spawnPoints);
		}

		public void Init()
		{
			playerBuilderController.Init();
		}

		public void Deinit()
		{
			playerBuilderController.Deinit();
		}
	}
}