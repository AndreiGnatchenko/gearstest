﻿using UnityEngine;

namespace Game
{
	public class PlayerWeaponController
	{
		private PlayerModel playerModel;
		private PlayerHealthController healthController;

		public PlayerWeaponController(PlayerModel playerModel, PlayerHealthController healthController)
		{
			this.playerModel = playerModel;
			this.healthController = healthController;
		}

		public void Attack(PlayerModel enemy)
		{
			if (playerModel.IsDead)
			{
				return;
			}

			if (enemy.IsDead)
			{
				return;
			}

			var damage = CalculateDamage();

			var validDamage = enemy.Controller.HealthController.Damage(damage);

			HandleVampire(validDamage);

			playerModel.PublishHit();
		}

		private void HandleVampire(int damage)
		{
			var vampireRatio = (float)playerModel.GetStat(StatsId.LIFE_STEAL_ID).Value / 100f;

			var resultHeal = Mathf.RoundToInt(-damage * vampireRatio);

			healthController.Heal(resultHeal);
		}

		private int CalculateDamage()
		{
			return playerModel.GetStat(StatsId.DAMAGE_ID).Value;
		}
	}
}
