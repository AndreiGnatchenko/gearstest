﻿using UnityEngine;

namespace Game
{
	public class PlayerHealthController
	{
		private PlayerModel playerModel;

		public PlayerHealthController(PlayerModel playerModel)
		{
			this.playerModel = playerModel;
		}

		public int Damage(int damage)
		{
			if (playerModel.IsDead)
			{
				return 0;
			}

			var healthDamage = damage - GetArmorDamage(damage);

			var currentHealth = playerModel.GetStat(StatsId.LIFE_ID).Value;

			var validDamage = -Mathf.Min(currentHealth, Mathf.Abs(healthDamage));

			playerModel.GetStat(StatsId.LIFE_ID).Value += validDamage;

			return validDamage;
		}

		public void Heal(int amount)
		{
			var currentHealth = playerModel.GetStat(StatsId.LIFE_ID).Value;

			var validHeal = Mathf.Min(playerModel.InitialHealth - currentHealth, amount);

			playerModel.GetStat(StatsId.LIFE_ID).Value += validHeal;
		}

		private int GetArmorDamage(int damage)
		{
			var damageRatio = (float)playerModel.GetStat(StatsId.ARMOR_ID).Value / 100f;

			return Mathf.RoundToInt(damage * damageRatio);
		}
	}
}
