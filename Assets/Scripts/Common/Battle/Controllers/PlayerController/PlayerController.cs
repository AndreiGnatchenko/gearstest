﻿using UnityEngine;

namespace Game
{
	public class PlayerController : MonoBehaviour
	{
		[SerializeField] private PlayerSkinController skinController;

		public PlayerHealthController HealthController { get; private set; }
		public PlayerWeaponController WeaponController { get; private set; }

		public void Init(PlayerModel playerModel)
		{
			HealthController = new PlayerHealthController(playerModel);
			WeaponController = new PlayerWeaponController(playerModel, HealthController);

			skinController.Init(playerModel);
		}

		public void Deinit()
		{
			skinController.Deinit();
		}
	}
}
