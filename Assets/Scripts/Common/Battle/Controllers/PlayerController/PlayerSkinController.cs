﻿using UnityEngine;

namespace Game
{
	public class PlayerSkinController : MonoBehaviour
	{
		[SerializeField] private AudioSource hitSource;
		[SerializeField] private AudioSource dieSource;
		[SerializeField] private Animator animator;

		private const string IsDeadKey = "IsDead";
		private const string AttackedKey = "Attack";

		private PlayerModel playerModel;

		public void Init(PlayerModel playerModel)
		{
			this.playerModel = playerModel;

			playerModel.GetStat(StatsId.LIFE_ID).Changed += OnHealthChanged;
			playerModel.Hit += OnHit;
		}

		public void Deinit()
		{
			playerModel.GetStat(StatsId.LIFE_ID).Changed -= OnHealthChanged;
			playerModel.Hit -= OnHit;
		}

		private void OnHealthChanged(int currentHealth)
		{
			animator.SetBool(IsDeadKey, playerModel.IsDead);

			if (playerModel.IsDead)
			{
				dieSource.Stop();
				dieSource.Play();
			}
		}

		private void OnHit()
		{
			animator.SetTrigger(AttackedKey);

			hitSource.Stop();
			hitSource.Play();
		}
	}
}
