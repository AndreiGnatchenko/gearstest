﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
	[SerializeField] private AnimationCurve zoomCurve;
	[SerializeField] private Transform handle;
	[SerializeField] private Camera battleCamera;

	private Data gameContent;

	public void Init(Data gameContent)
	{
		this.gameContent = gameContent;

		battleCamera.transform.localPosition = Vector3.back * gameContent.cameraSettings.roundRadius;
		handle.transform.localPosition = Vector3.up * gameContent.cameraSettings.height;

		var cameraPosition = battleCamera.transform.position;
		var lookAtPosition = Vector3.up * gameContent.cameraSettings.lookAtHeight;

		var cameraDir = lookAtPosition - cameraPosition;
		battleCamera.transform.rotation = Quaternion.LookRotation(cameraDir);
	}

	public void Deinit()
	{
	}

	private void Update()
	{
		var rotation = 360f / gameContent.cameraSettings.roundDuration * Time.deltaTime;

		handle.Rotate(Vector3.up, rotation);

		HandleZoom();
	}

	private void HandleZoom()
	{
		var ratio = (Time.time % gameContent.cameraSettings.fovDuration) / gameContent.cameraSettings.fovDuration;

		var evaluated = zoomCurve.Evaluate(ratio);

		var zoom = Mathf.Lerp(gameContent.cameraSettings.fovMin, gameContent.cameraSettings.fovMax, evaluated);

		battleCamera.fieldOfView = zoom;
	}
}
