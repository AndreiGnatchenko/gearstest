﻿using UnityEngine;

namespace Game
{
    public class SpawnController
    {
		private PlayerController playerPrefab;

		public SpawnController(PlayerController playerPrefab)
		{
			this.playerPrefab = playerPrefab;
		}

        public PlayerController SpawnPlayer(Transform spawnPoint)
		{
			var instance = GameObject.Instantiate(playerPrefab, spawnPoint);

			instance.transform.localPosition = Vector3.zero;
			instance.transform.localRotation = Quaternion.identity;

			return instance;
		}

        public void DespawnPlayer(PlayerController instance)
		{
			GameObject.Destroy(instance.gameObject);
		}
    }
}