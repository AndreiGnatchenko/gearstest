﻿namespace Game
{
	public interface IIdGenerator
	{
		int GenerateId();
	}

	public class IdGenerator : IIdGenerator
	{
		private int counter;

		public int GenerateId()
		{
			return counter++;
		}
	}
}