﻿
using Support;
using System.Collections.Generic;

namespace Game
{
	public class BuffController
	{
		private Data gameContent;
		private bool useBuffs;

		public BuffController(Data gameContent, bool useBuffs)
		{
			this.gameContent = gameContent;
			this.useBuffs = useBuffs;
		}

		public List<PlayerInitData> GetPlayersStats(int playersCount, List<Stat> initialStats)
		{
			var playerDatas = new List<PlayerInitData>();

			for (var i = 0; i < playersCount; i++)
			{
				var copy = CopyStats(initialStats);

				var buffsCount = UnityEngine.Random.Range(gameContent.settings.buffCountMin, gameContent.settings.buffCountMax + 1);

				if (!useBuffs) buffsCount = 0;

				var buffs = GetBuffs(buffsCount, gameContent.settings.allowDuplicateBuffs);

				ApplyBuffs(copy, buffs);

				playerDatas.Add(new PlayerInitData()
				{
					initialStats = copy,
					buffs = buffs
				});
			}

			return playerDatas;
		}

		private void ApplyBuffs(List<Stat> copy, List<Buff> buffs)
		{
			foreach (var buff in buffs)
			{
				ApplyBuff(copy, buff);
			}
		}

		private void ApplyBuff(List<Stat> copy, Buff buff)
		{
			foreach (var stat in buff.stats)
			{
				var copyStat = copy.Find(item => item.id == stat.statId);

				copyStat.value += stat.value;
			}
		}

		private List<Buff> GetBuffs(int buffsCount, bool allowDuplicateBuffs)
		{
			return allowDuplicateBuffs ? GetNotUniqueBuffs(buffsCount) : GetUniqueBuffs(buffsCount);
		}

		private List<Buff> GetUniqueBuffs(int buffsCount)
		{
			var buffsList = new List<Buff>(gameContent.buffs);

			buffsList.Shuffle();

			var validCount = UnityEngine.Mathf.Min(buffsCount, buffsList.Count);

			return buffsList.GetRange(0, validCount);
		}

		private List<Buff> GetNotUniqueBuffs(int buffsCount)
		{
			var buffs = new List<Buff>();

			for (var i = 0; i < buffsCount; i++)
			{
				buffs.Add(gameContent.buffs.Random());
			}

			return buffs;
		}

		private List<Stat> CopyStats(List<Stat> stats)
		{
			var copy = new List<Stat>();

			foreach (var stat in stats)
			{
				var instance = new Stat()
				{
					id = stat.id,
					icon = stat.icon,
					title = stat.title,
					value = stat.value
				};

				copy.Add(instance);
			}

			return copy;
		}
	}
}