﻿using System.Collections.Generic;

namespace Game
{
	public interface INickGenerator
	{
		string GenerateNick();
	}

	public class NickGenerator : INickGenerator
	{
		private List<string> nicknames = new List<string>()
		{
			"Bob",
			"Dave",
			"Sally",
			"Garry",
			"Pasha"
		};

		public string GenerateNick()
		{
			var randomIndex = UnityEngine.Random.Range(0, nicknames.Count);

			return nicknames[randomIndex];
		}
	}
}