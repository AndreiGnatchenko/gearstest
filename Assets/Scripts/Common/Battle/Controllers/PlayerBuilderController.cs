﻿using Support;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class PlayerBuilderController
    {
		private List<PlayerModel> players;
		private SpawnController spawnController;
		private List<Transform> spawnPoints;

		private List<PlayerController> instances = new List<PlayerController>();

		public PlayerBuilderController(List<PlayerModel> players, SpawnController spawnController, List<Transform> spawnPoints)
		{
			this.players = players;
			this.spawnController = spawnController;
			this.spawnPoints = spawnPoints;
		}

		public void Init()
		{
			var spawnPoints = GetSpawnPoints();

			for (var i = 0; i < players.Count; i++)
			{
				var playerModel = players[i];

				var validSpawnIndex = i % spawnPoints.Count;

				var spawnPoint = spawnPoints[validSpawnIndex];

				var instance = spawnController.SpawnPlayer(spawnPoint);

				instance.Init(playerModel);

				playerModel.Controller = instance;

				instances.Add(instance);
			}
		}

		public void Deinit()
		{
			foreach (var instance in instances)
			{
				instance.Deinit();

				spawnController.DespawnPlayer(instance);
			}

			instances.Clear();
		}

		private List<Transform> GetSpawnPoints()
		{
			return spawnPoints.Shuffle();
		}
	}
}