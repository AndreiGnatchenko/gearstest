﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
	public class PlayerHealthView : MonoBehaviour
	{
		[SerializeField] private Color positive = Color.green;
		[SerializeField] private Color negative = Color.red;
		[SerializeField] private Text damageText;
		[SerializeField] private Text healthText;
		[SerializeField] private Animation healthChangeAnim;
		[SerializeField] private Image healthBar;

		private PlayerModel model;

		public void Init(PlayerModel model)
		{
			this.model = model;

			model.GetStat(StatsId.LIFE_ID).Difference += OnHealthChanged;

			OnHealthChanged(0, model.GetStat(StatsId.LIFE_ID).Value);
		}

		public void Deinit()
		{
			model.GetStat(StatsId.LIFE_ID).Difference -= OnHealthChanged;
		}

		private void OnHealthChanged(int previousHealth, int currentHealth)
		{
			var difference = currentHealth - previousHealth;

			if (difference == 0)
			{
				return;
			}

			var ratio = (float)currentHealth / model.InitialHealth;

			damageText.color = difference > 0 ? positive : negative;
			healthBar.fillAmount = ratio;

			damageText.text = difference.ToString();
			healthText.text = currentHealth.ToString();

			healthChangeAnim.Stop();
			healthChangeAnim.Play();
		}
	}
}