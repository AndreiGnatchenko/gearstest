﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class PlayerBuffViews : MonoBehaviour
	{
		[SerializeField] private Transform viewAnchor;
		[SerializeField] private PlayerBuffView viewPrefab;

		private List<PlayerBuffView> views = new List<PlayerBuffView>();

		public void Init(PlayerModel player)
		{
			foreach (var buff in player.Buffs)
			{
				var view = Instantiate(viewPrefab, viewAnchor);

				view.Init(buff);

				views.Add(view);
			}
		}

		public void Deinit()
		{
			foreach (var view in views)
			{
				view.Deinit();

				Destroy(view.gameObject);
			}

			views.Clear();
		}
	}
}