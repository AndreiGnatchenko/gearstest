﻿using UnityEngine;

namespace Game
{
	public class CurrentPlayerView : MonoBehaviour
	{
		[SerializeField] private Transform token;

		private BattleModel battleModel;

		public void Init(BattleModel battleModel)
		{
			this.battleModel = battleModel;

			battleModel.CurrentPlayer.Changed += OnCurrentPlayerChanged;

			OnCurrentPlayerChanged(battleModel.CurrentPlayer.Value);
		}

		public void Deinit()
		{
			battleModel.CurrentPlayer.Changed -= OnCurrentPlayerChanged;
		}

		private void OnCurrentPlayerChanged(int currentPlayerId)
		{
			var currentPlayer = battleModel.Players.Find(player => player.Id == currentPlayerId);

			var position = currentPlayer.Controller.transform.position;

			token.position = position;
		}
	}
}