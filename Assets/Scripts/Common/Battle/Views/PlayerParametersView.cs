﻿using UnityEngine;

namespace Game
{
	public class PlayerParametersView : MonoBehaviour
	{
		[SerializeField] private PlayerHealthView healthView;

		private PlayerModel model;
		private Camera battleCamera;

		private readonly Vector3 Shift = Vector3.up * 5f;

		public void Init(PlayerModel model, Data gameContent, Camera battleCamera)
		{
			this.model = model;
			this.battleCamera = battleCamera;

			healthView.Init(model);
		}

		public void Deinit()
		{
			healthView.Deinit();
		}

		public void ExternalUpdate()
		{
			var screenPosition = battleCamera.WorldToScreenPoint(model.Controller.transform.position + Shift);

			transform.position = screenPosition;
		}
	}
}