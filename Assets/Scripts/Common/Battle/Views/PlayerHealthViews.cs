﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class PlayerHealthViews : MonoBehaviour
	{
		[SerializeField] private Transform viewAnchor;
		[SerializeField] private PlayerHealthView viewPrefab;

		private List<PlayerHealthView> views = new List<PlayerHealthView>();

		public void Init(List<PlayerModel> players)
		{
			foreach (var player in players)
			{
				var view = Instantiate(viewPrefab, viewAnchor);

				view.Init(player);

				views.Add(view);
			}
		}

		public void Deinit()
		{
			foreach (var view in views)
			{
				view.Deinit();

				Destroy(view.gameObject);
			}

			views.Clear();
		}
	}
}