﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
	public class PlayerBuffView : MonoBehaviour
	{
		[SerializeField] private Image icon;
		[SerializeField] private Text title;

		public void Init(Buff content)
		{
			icon.sprite = Resources.Load<Sprite>(content.icon);
			title.text = content.title;
		}

		public void Deinit()
		{
		}
	}
}