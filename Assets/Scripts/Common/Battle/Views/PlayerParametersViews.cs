﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class PlayerParametersViews : MonoBehaviour
	{
		[SerializeField] private Transform viewAnchor;
		[SerializeField] private PlayerParametersView viewPrefab;

		private List<PlayerParametersView> views = new List<PlayerParametersView>();
		private bool inited;

		public void Init(List<PlayerModel> players, Data gameContent, Camera battleCamera)
		{
			foreach (var player in players)
			{
				var view = Instantiate(viewPrefab, viewAnchor);

				view.Init(player, gameContent, battleCamera);

				views.Add(view);
			}

			inited = true;
		}

		public void Deinit()
		{
			foreach (var view in views)
			{
				view.Deinit();

				Destroy(view.gameObject);
			}

			views.Clear();

			inited = false;
		}

		private void Update()
		{
			if (!inited)
			{
				return;
			}

			views.ForEach(view => view.ExternalUpdate());
		}
	}
}