﻿using Support;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
	public class PlayerStatView : MonoBehaviour
	{
		[SerializeField] private Image icon;
		[SerializeField] private Text counter;
		[SerializeField] private Text title;

		private ReactiveProperty<int> reactive;

		public void Init(Stat content, ReactiveProperty<int> reactive)
		{
			this.reactive = reactive;

			icon.sprite = Resources.Load<Sprite>(content.icon);
			title.text = content.title;

			reactive.Changed += OnStatChanged;
			OnStatChanged(reactive.Value);
		}

		public void Deinit()
		{
			reactive.Changed -= OnStatChanged;
		}

		private void OnStatChanged(int value)
		{
			counter.text = Mathf.Abs(value).ToString();
		}
	}
}