﻿using UnityEngine;

namespace Game
{
	public class UIParametersPanel : MonoBehaviour
	{
		[SerializeField] private PlayerStatViews statViews;
		[SerializeField] private PlayerBuffViews buffViews;

		private BattleModel battleModel;
		private Data gameContent;
		private bool inited;

		public void Init(BattleModel battleModel, Data gameContent)
		{
			this.battleModel = battleModel;
			this.gameContent = gameContent;

			battleModel.CurrentPlayer.Changed += OnCurrentPlayerChanged;

			OnCurrentPlayerChanged(battleModel.CurrentPlayer.Value);

			inited = true;
		}

		public void Deinit()
		{
			if (!inited)
			{
				return;
			}

			battleModel.CurrentPlayer.Changed -= OnCurrentPlayerChanged;

			InternalDeinit();

			inited = false;
		}

		private void OnCurrentPlayerChanged(int currentPlayerId)
		{
			var currentPlayerModel = battleModel.Players.Find(item => item.Id == currentPlayerId);

			InternalDeinit();
			InternalInit(currentPlayerModel);
		}

		private void InternalInit(PlayerModel model)
		{
			statViews.Init(model, gameContent);
			buffViews.Init(model);
		}

		private void InternalDeinit()
		{
			statViews.Deinit();
			buffViews.Deinit();
		}
	}
}
