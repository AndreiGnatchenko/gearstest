﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class PlayerStatViews : MonoBehaviour
	{
		[SerializeField] private Transform viewAnchor;
		[SerializeField] private PlayerStatView viewPrefab;

		private List<PlayerStatView> views = new List<PlayerStatView>();

		public void Init(PlayerModel player, Data gameContent)
		{
			foreach (var pair in player.CurrentStats)
			{
				var content = gameContent.stats.Find(item => item.id == pair.Key);

				var view = Instantiate(viewPrefab, viewAnchor);

				view.Init(content, pair.Value);

				views.Add(view);
			}
		}

		public void Deinit()
		{
			foreach (var view in views)
			{
				view.Deinit();

				Destroy(view.gameObject);
			}

			views.Clear();
		}
	}
}