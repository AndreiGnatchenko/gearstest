﻿using Support;
using System;
using System.Collections.Generic;

namespace Game
{
	public class BattleModel
	{
		public List<PlayerModel> Players { get; }

		public ReactiveProperty<int> CurrentPlayer = new ReactiveProperty<int>();

		public BattleModel(List<PlayerModel> players, int currentPlayer)
		{
			Players = players;
			CurrentPlayer.Value = currentPlayer;
		}
	}
}