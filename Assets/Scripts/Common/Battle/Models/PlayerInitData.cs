﻿using System.Collections.Generic;

namespace Game
{
	public class PlayerInitData
	{
		public List<Stat> initialStats;
		public List<Buff> buffs;
	}
}