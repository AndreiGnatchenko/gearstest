﻿using Support;
using System;
using System.Collections.Generic;

namespace Game
{
	public class PlayerModel
	{
		public event Action Hit;

		public int Id { get; }
		public string Nick { get; }
		public bool IsDead => GetStat(StatsId.LIFE_ID).Value <= 0;
		public int InitialHealth { get; }

		public Dictionary<int, ReactiveProperty<int>> CurrentStats { get; }
		public List<Buff> Buffs { get; }
		public PlayerController Controller { get; set; }

		public PlayerModel(int id, string nick, PlayerInitData playerData)
		{
			Id = id;
			Nick = nick;

			CurrentStats = CreateCurrentStats(playerData.initialStats);
			Buffs = playerData.buffs;
			InitialHealth = GetStat(StatsId.LIFE_ID).Value;
		}

		public ReactiveProperty<int> GetStat(int statId)
		{
			CurrentStats.TryGetValue(statId, out var value);

			return value;
		}

		public void SetStat(int statId, int value)
		{
			CurrentStats[statId].Value = value;
		}

		private Dictionary<int, ReactiveProperty<int>> CreateCurrentStats(List<Stat> stats)
		{
			var map = new Dictionary<int, ReactiveProperty<int>>();

			foreach (var stat in stats)
			{
				var reactive = new ReactiveProperty<int>();

				reactive.Value = stat.value;

				map.Add(stat.id, reactive);
			}

			return map;
		}

		public void PublishHit()
		{
			Hit?.Invoke();
		}
	}
}