﻿using UnityEngine;

namespace Game
{
    public interface IContentLoader
	{
        string LoadContent();
	}

	public class ResourceContentLoader : IContentLoader
	{
		private readonly string path;

		public ResourceContentLoader(string path)
		{
			this.path = path;
		}

		public string LoadContent()
		{
			var textAsset = Resources.Load<TextAsset>(path);

			return textAsset.text;
		}
	}
}