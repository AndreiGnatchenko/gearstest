﻿using System;
using System.Collections.Generic;

[Serializable]
public class Data
{
    public CameraModel cameraSettings;
    public GameModel settings;
    public List<Stat> stats;
    public List<Buff> buffs;
}

[Serializable]
public class CameraModel
{
    public float roundDuration;
    public float roundRadius;
    public float height;
    public float lookAtHeight;
    public float roamingRadius;
    public float roamingDuration;
    public float fovMin;
    public float fovMax;
    public float fovDelay;
    public float fovDuration;
}

[Serializable]
public class GameModel
{
    public int playersCount;
    public int buffCountMin;
    public int buffCountMax;
    public bool allowDuplicateBuffs;
}

[Serializable]
public class Stat
{
    public int id;
    public string title;
    public string icon;
    public int value;
}

[Serializable]
public class BuffStat
{
    public int value;
    public int statId;
}

[Serializable]
public class Buff
{
    public string icon;
    public int id;
    public string title;
    public List<BuffStat> stats;
}
