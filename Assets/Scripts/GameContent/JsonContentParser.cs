﻿using Newtonsoft.Json;

namespace Game
{
    public interface IGameContentParser
	{
        Data ParseContent();
	}

	public class JsonContentParser : IGameContentParser
	{
		private string rawData;

		public JsonContentParser(string rawData)
		{
			this.rawData = rawData;
		}

		public Data ParseContent()
		{
			return JsonConvert.DeserializeObject<Data>(rawData);
		}
	}
}